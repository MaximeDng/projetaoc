package com.istic.aoc.ui;

import com.istic.aoc.activeObject.callable.Generate;
import com.istic.aoc.activeObject.generator.Generator;
import com.istic.aoc.activeObject.generator.GeneratorImpl;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.text.Text;
import com.istic.aoc.activeObject.observer.AfficheurImpl;
import com.istic.aoc.activeObject.proxy.Canal;
import com.istic.aoc.activeObject.strategy.Strategy;
import com.istic.aoc.activeObject.strategy.StrategyAtomique;
import com.istic.aoc.activeObject.strategy.StrategySequentielle;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Controller {

    private ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);

    /**********************************FXML Part************************************/
    @FXML
    private TextArea taAffich1;
    @FXML
    private TextArea taAffich2;
    @FXML
    private TextArea taAffich3;
    @FXML
    private TextArea taAffich4;

    @FXML
    private Text strategyValue;

    @FXML
    private void setSequentielle(){
        strategyValue.setText("Strategy : Sequentielle");
        setStrategy(new StrategySequentielle());
    }

    @FXML
    protected void setAtomique(){
        strategyValue.setText("Strategy : Atomique");
        setStrategy(new StrategyAtomique());
    }

    private AfficheurImpl modelAffich1;
    private AfficheurImpl modelAffich2;
    private AfficheurImpl modelAffich3;
    private AfficheurImpl modelAffich4;
    private Generator gen;

    @FXML
    public void initialize() {

        initializeModel();

        taAffich1.textProperty().bind(modelAffich1.trace);
        taAffich2.textProperty().bind(modelAffich2.trace);
        taAffich3.textProperty().bind(modelAffich3.trace);
        taAffich4.textProperty().bind(modelAffich4.trace);
    }

    public void initializeModel() {
        gen = new GeneratorImpl();

        modelAffich1 = new AfficheurImpl();
        modelAffich1.initTrace("0 latence");
        modelAffich2 = new AfficheurImpl();
        modelAffich2.initTrace("25 latence");
        modelAffich3 = new AfficheurImpl();
        modelAffich3.initTrace("100 latence");
        modelAffich4 = new AfficheurImpl();
        modelAffich4.initTrace("150 latence");

        Canal Canal1 = new Canal(gen,0);
        Canal1.attach(modelAffich1);
        Canal Canal2 = new Canal(gen,25);
        Canal2.attach(modelAffich2);
        Canal Canal3 = new Canal(gen,100);
        Canal3.attach(modelAffich3);
        Canal Canal4 = new Canal(gen,150);
        Canal4.attach(modelAffich4);

        gen.attach(Canal1);
        gen.attach(Canal2);
        gen.attach(Canal3);
        gen.attach(Canal4);

        setStrategy(new StrategySequentielle());
        strategyValue.setText("Strategie : Sequentielle");

        startGenerator();

    }

    private void setStrategy(Strategy strategy){
        gen.changeStrategy(strategy);
    }

    private void startGenerator(){
        scheduledExecutorService.scheduleAtFixedRate(new Generate(gen), 0, 1000,TimeUnit.MILLISECONDS);
    }
}
