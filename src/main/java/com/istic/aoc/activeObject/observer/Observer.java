package com.istic.aoc.activeObject.observer;

public interface Observer<T> {
    void update(T subject);
}
