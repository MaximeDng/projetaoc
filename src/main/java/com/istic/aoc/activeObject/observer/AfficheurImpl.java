package com.istic.aoc.activeObject.observer;

import com.istic.aoc.activeObject.generator.AsyncGenerator;
import javafx.beans.property.SimpleStringProperty;


public class AfficheurImpl implements Afficheur {

    public SimpleStringProperty trace;

    @Override
    public void update(AsyncGenerator subject) {
        try {
            addTrace(subject.getValue().get());
        }catch (Exception ex){
            System.out.println("Erreur lors de l'execution de la méthode update() d'un afficheur");
        }
    }

    private void addTrace(int newVal){
        trace.set(newVal + "\n"+ trace.getValue());
    }

    public void initTrace(String value) {
        trace = new SimpleStringProperty(value);
    }
}
