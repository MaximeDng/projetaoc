package com.istic.aoc.activeObject.observer;

import java.util.concurrent.Future;

public interface AsyncObserver<T> {
    Future<Void> update(T subject);
}
