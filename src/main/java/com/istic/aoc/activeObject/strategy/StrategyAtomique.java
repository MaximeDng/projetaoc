package com.istic.aoc.activeObject.strategy;

import com.istic.aoc.activeObject.generator.Generator;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class StrategyAtomique implements Strategy {

    private Generator generator;
    private List<Future<Void>> futures = new ArrayList<>();


    /*@Override
    public void execute() {
        this.com.istic.aoc.activeObject.generator.getObservers().forEach(obs-> {
            try {
                obs.update(com.istic.aoc.activeObject.generator).get();
            }catch(Exception ex){
                System.out.println("Une erreur s'est produite lors de l'éxecution de la méthode update (Strategy Atomique)");
            }
        });
    }*/

    @Override
    public void execute() {
        this.generator.getObservers().forEach(canal ->{
          Future<Void> future = canal.update(generator);
          if (future != null){
              this.futures.add(future);
          }
        });

        this.futures.forEach(future ->{
            try {
                future.get();
            } catch (InterruptedException | ExecutionException e) {
                System.out.println("Une erreur s'est produite lors de l'éxecution de la méthode update (Strategy Atomique)");
            }
        });

        this.futures.clear();
    }

    @Override
    public void generator(Generator generator) {
        this.generator = generator;
    }
}
