package com.istic.aoc.activeObject.strategy;

/**
 * Interface qui est à implémenter dans la classe utilisant une stratégie
 */
public interface ClientStrategy {
    /**
     * Permet de changer la stratégie
     * @param strategy
     */
    void changeStrategy(Strategy strategy);
}
