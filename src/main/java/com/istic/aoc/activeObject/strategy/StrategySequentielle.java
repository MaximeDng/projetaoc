package com.istic.aoc.activeObject.strategy;

import com.istic.aoc.activeObject.generator.Generator;

public class StrategySequentielle implements Strategy {
    private Generator generator;

    @Override
    public void execute() {
        this.generator.getObservers().forEach(obs -> obs.update(generator));
    }

    @Override
    public void generator(Generator generator) {
        this.generator = generator;
    }
}
