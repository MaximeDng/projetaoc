package com.istic.aoc.activeObject.strategy;

import com.istic.aoc.activeObject.generator.Generator;

/**
 * Interface com.istic.aoc.activeObject.strategy
 */
public interface Strategy {

    /**
     * Méthode d'éxecution de la Stratégy
     */
       void execute();

    /**
     * Méthode qui permet de configurer la Strategy
     * @param generator
     */
    void generator(Generator generator);
}
