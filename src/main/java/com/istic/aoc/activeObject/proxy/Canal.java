package com.istic.aoc.activeObject.proxy;

import com.istic.aoc.activeObject.callable.GetValue;
import com.istic.aoc.activeObject.callable.Update;
import com.istic.aoc.activeObject.generator.AsyncGenerator;
import com.istic.aoc.activeObject.generator.Generator;
import com.istic.aoc.activeObject.generator.Subject;
import com.istic.aoc.activeObject.observer.AsyncObserver;
import com.istic.aoc.activeObject.observer.Observer;

import java.util.LinkedList;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Canal implements Subject<AsyncGenerator>, AsyncGenerator, AsyncObserver<Generator> {

    private ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(Integer.MAX_VALUE);
    private LinkedList<Observer<AsyncGenerator>> observers = new LinkedList<>();
    private Generator generator;
    private int latency;

    public Canal(Generator generator, int latency){
        this.generator = generator;
        this.latency = latency;
    }


    /****************************Subject****************************/
    @Override
    public void attach(Observer<AsyncGenerator> observer) {
        this.observers.add(observer);
    }

    @Override
    public void detach(Observer<AsyncGenerator> observer) {
        //à implémenter si nécessaire
    }

    @Override
    public void notifyObservers() {
        this.observers.forEach(obs -> obs.update(this));
    }

    /*******************************END*****************************/


    /*****************************AsyncObserver************************/
    @Override
    public Future<Void> update(Generator subject) {
        return scheduledExecutorService.schedule(new Update(this), latency, TimeUnit.MILLISECONDS);

    }

    /*******************************END*****************************/


    /*****************************AsyncGenerator************************/
    @Override
    public Future<Integer> getValue() {
        return scheduledExecutorService.schedule(new GetValue(this.generator), latency, TimeUnit.MILLISECONDS);
    }
    /*******************************END*****************************/


}
