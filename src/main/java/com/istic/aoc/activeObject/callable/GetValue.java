package com.istic.aoc.activeObject.callable;

import com.istic.aoc.activeObject.generator.Generator;

import java.util.concurrent.Callable;

/**
 * Cette classe permet de récuperer la valeur du génerateur
 */
public class GetValue implements Callable<Integer> {
    private Generator generator;

    /**
     * Constructeur du callable
     * @param generator
     */
    public GetValue(Generator generator) {
        this.generator = generator;
    }

    /**
     *
     * @return La valeur Integer du génerateur
     */
    @Override
    public Integer call(){
        return this.generator.getValue();
    }
}
