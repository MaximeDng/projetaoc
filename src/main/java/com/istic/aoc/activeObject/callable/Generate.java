package com.istic.aoc.activeObject.callable;

import com.istic.aoc.activeObject.generator.Generator;

import java.util.concurrent.Callable;

/**
 * Cette classe à pour but de démarrer le génerateur
 */
public class Generate implements Runnable {

    private Generator generator;

    public Generate(Generator generator) {
        this.generator = generator;
    }

    @Override
    public void run() {
         generator.generate();
    }
}
