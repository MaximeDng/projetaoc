package com.istic.aoc.activeObject.callable;

import com.istic.aoc.activeObject.generator.AsyncGenerator;
import com.istic.aoc.activeObject.generator.Subject;

import java.util.concurrent.Callable;

/**
 * Cette classe notifie la disponibilité d'une nouvelle valeur de la part du génerateur
 */
public class Update implements Callable<Void> {

    private Subject<AsyncGenerator> subject;

    /**
     * Constructeur du callable
     * @param subject
     */
    public Update(Subject<AsyncGenerator> subject) {
        this.subject = subject;
    }

    /**
     * Notifie les observeurs du changement de valeur du génerateur
     * @return vide
     */
    @Override
    public Void call() {
        this.subject.notifyObservers();
        return null;
    }
}
