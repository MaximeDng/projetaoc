package com.istic.aoc.activeObject.generator;

import com.istic.aoc.activeObject.observer.AsyncObserver;
import com.istic.aoc.activeObject.strategy.ClientStrategy;

import java.util.LinkedList;

public interface Generator extends AsyncSubject<Generator>, ClientStrategy {

    /**
     * Méthode qui permet de récuperer la valeur generée
     * @return Integer
     */
    Integer getValue();

    /**
     * Méthode qui permet de génerer une valeur
     */
    void generate();

    /**
     * Méthode qui retourne une liste contenant les observateurs
     * @return
     */
    LinkedList<AsyncObserver<Generator>> getObservers();
}
