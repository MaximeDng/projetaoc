package com.istic.aoc.activeObject.generator;

import com.istic.aoc.activeObject.observer.AsyncObserver;
import com.istic.aoc.activeObject.strategy.Strategy;

import java.util.LinkedList;

public class GeneratorImpl implements Generator {

    private Strategy strategy;
    private int value = 1;
    private LinkedList<AsyncObserver<Generator>> observers = new LinkedList<>();

    /************************** Partie generation******************************/
    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public void generate() {
        this.value++;
        this.notifyObservers();
    }

    /************************* Fin ******************************************/


    /************************ Partie observeurs ****************************/
    @Override
    public LinkedList<AsyncObserver<Generator>> getObservers() {
        return observers;
    }

    @Override
    public void attach(AsyncObserver<Generator> observer) {
        this.observers.add(observer);
    }

    @Override
    public void detach(AsyncObserver<Generator> observer) throws Exception {
        this.observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        this.strategy.execute();
    }

    /*************************** Fin *****************************************/

    /*************************** Strategy ************************************/

    @Override
    public void changeStrategy(Strategy strategy) {
        this.strategy = strategy;
        this.strategy.generator(this);
    }

    /*************************** Fin ******************************************/
}
