package com.istic.aoc.activeObject.generator;

import com.istic.aoc.activeObject.observer.AsyncObserver;

public interface AsyncSubject<T> {
    /**
     * Permet d'attacher un nouvel observeur
     * @param observer
     */
    void attach(AsyncObserver<T> observer);

    /**
     * Permet de retirer un observeur
     * @param observer
     * @throws Exception
     */
    void detach(AsyncObserver<T> observer)throws Exception;

    /**
     * Permet de notifier les observeurs
     */
    void notifyObservers();
}
