package com.istic.aoc.activeObject.generator;

import com.istic.aoc.activeObject.observer.Observer;

public interface Subject<T> {
    /**
     * Permet d'attacher un observeur
     * @param observer
     */
    void attach(Observer<T> observer);

    /**
     * Permet de retirer un observeur
     * @param observer
     */
    void detach(Observer<T> observer);

    /**
     * Permet de notifier les observeurs
     */
    void notifyObservers();
}
