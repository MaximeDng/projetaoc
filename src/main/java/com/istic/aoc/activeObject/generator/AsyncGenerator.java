package com.istic.aoc.activeObject.generator;

import java.util.concurrent.Future;

public interface AsyncGenerator {


    /**
     * Cette méthode permet d'obtenir la valeur generée
     * @return Integer
     */
    Future<Integer> getValue();
}
